import math
from euclidean_distance import EuclideanDistance


class AdaptationSOM:
    """
    This class handle adaptation self-organizing map
    Thi will decrease :
    1. learning rate
    2. radius : Neighborhood set (this value
    """
    learning_rate_start = 0.3
    learning_rate_end = 0.01
    current_learning_rate = 0.3
    radius_start = 5
    radius_end = 1
    current_radius = 5

    def adapt(self, winner, time, maxtime):
        """
            Stochastic approximation method to decrease learning rate
        """
        shrink = 1.0 - ((1.0 * time) / maxtime)
        self._set_learning_rate(
            self.learning_rate_start * shrink)
        self._set_radius(self.radius_start * shrink)

    def get_learning_rate(self):
        return self.current_learning_rate

    def get_radius(self):
        return self.current_radius

    def _set_learning_rate(self, learning_rate):
        if learning_rate >= self.learning_rate_end:
            self.current_learning_rate = learning_rate

    def end_learning(self):
        return self.current_learning_rate <= self.learning_rate_end

    def _set_radius(self, radius):
        if radius >= self.radius_end:
            self.current_radius = radius

    def get_addaption_radius(self, time, maxtime):
        return self.radius_start * (1.0 - ((time * 1.0) / maxtime))
