from distance import Distance
import math


class EuclideanDistance(Distance):

    def get_distance(self, x, y):
        """
        Euclidean Distance implementation
        :param x: float array
        :param y: float array
        :return: float distance array x and y
        """
        #Add validation to make sure x and y is a list
        distance = 0
        for i in range(0, len(x) - 1):
            point_distance = x[i] - y[i]
            distance += point_distance * point_distance
        return math.sqrt(distance)

    def get_distance_2(self, point1, point2, dimensions):
        """
        point1, point2 and dimension adl array
        3,4 (stimulus
        8,5
        10,10
        1=min(8-2,3+10-2%10)
        3,4
        2,2
        4-2,4+10-2%
        """
        distance = 0
        for i in range(0, len(point1)):
            coord1 = dimensions[i] + point1[i] if point1[i] < 0 else point1[i]
            coord2 = dimensions[i] + point2[i] if point2[i] < 0 else point2[i]
            diff = min(abs(coord1 - coord2),
                       (coord1 + dimensions[i] - coord2) % dimensions[i])
            distance += diff * diff
        distance = math.sqrt(distance)
        return distance
