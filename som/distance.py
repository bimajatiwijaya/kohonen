
class Distance:

    def get_distance(self, x, y):
        """
        calculate distance between 2 float
        :param x: float
        :param y: float
        :return: float negative or positive
        """
        return x - y
