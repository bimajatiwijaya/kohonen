from adaptation import AdaptationSOM
from euclidean_distance import EuclideanDistance
import numpy as np

class KohonenNode(AdaptationSOM):
     """
     Kohonen node class
     """
     node = []
     distance = EuclideanDistance()

     def __init__(self, node):
          """
          Initial node in constructor
          :param weight:
          """
          self.node = np.array(node)

     def get_node(self):
          """ Return node """
          return self.node

     def _set_node(self, node):
          """ Update node weight """
          self.node = np.array(node)

     def update_weight1(self, stimulus):
         new_weight = self.node[:]
         for i in range(0, len(new_weight)):
             new_weight[i] += \
                 self.current_radius * self.current_learning_rate * (stimulus[i] - self.node[i])
         self._set_node(new_weight)

     def update_weight(self, stimulus, new_learning_rate):
         new_weight = self.node[:]
         stimulus = stimulus[:len(stimulus)-1]
         update = (1.0 - new_learning_rate)
         temp_weight = np.array([n * update for n in new_weight]) + \
                       np.array([n * new_learning_rate for n in stimulus])
         diff = self.distance.get_distance(new_weight, temp_weight)
         if diff > 0.5:
             self._set_node(temp_weight)

