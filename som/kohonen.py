from som.node import KohonenNode
from som.adaptation import AdaptationSOM
from som.euclidean_distance import EuclideanDistance
import random
import pandas as pd
import math


class KohonenNet:
    # List of kohonen node
    node = []
    training = None
    adaption = None
    _net_dimension = 2.0
    _net_dimensions = []
    _cube_counter = 0.0
    _cube_edge_length = 0.0
    _cube_lenght = 0.0
    _cube_offset = []
    _cube_edge_lengths = []
    _cube_offset_coordinate = []
    _data_dimention = 0
    training_time = 1000

    def __init__(self, data_dimention, dimention, dimentions, path_stimulus, time):
        if dimention > 2:
            raise Exception(
                'This program maximum handle 2 dimension.')
        self.adaption = AdaptationSOM()
        self.set_training_time(time)
        node_total = 1
        self._data_dimention = data_dimention
        self._net_dimension = dimention
        self._net_dimensions = dimentions
        for i in range(0, dimention):
            node_total *= dimentions[i]
        self.node = []
        # initial all kononenNode
        for i in range(0, node_total):
            random_weight = []
            for col in range(0, data_dimention + 1):
                random_weight.append(random.uniform(-1, 1))
            self.node.append(KohonenNode(random_weight))

        data = pd.read_csv(path_stimulus)
        store = pd.HDFStore('dataset.h5')
        store['mydata'] = data
        store.close()
        store = pd.HDFStore('dataset.h5')
        data = store['mydata']
        store.close()
        # Data in Numpy format then set to attribut training
        self.training = data.values

    def set_training_time(self, value):
        """
        Update training time if user want to update it
        :param value:
        :return:
        """
        self.training_time = value

    def som_impl(self):
        t = 1
        cluster = []
        while t <= self.training_time:
            cluster = []
            for stimulus in self.training:
                idx_best_node = self.get_winner(stimulus)
                stimulus_coor = self._get_coordinates_of_index(idx_best_node)
                # Save result SOM time to time
                cluster.append(dict(
                    stimulus=stimulus,
                    vector=self.node[int(idx_best_node)].get_node(),
                    cluster=stimulus_coor,))
                # adapting every node in range to stimulus
                shrink_range = 2 * int(
                    self.adaption.get_radius())
                self.cube(shrink_range, stimulus_coor)
                while self.cubehasnext():
                    current_node = self._cubeNext()
                    # update all neightboor
                    self.adaption.adapt(stimulus, t, self.training_time)
                    # adjusting weight of node
                    if int(current_node) < 0:
                        continue
                    self.node[int(current_node)].update_weight(
                        stimulus, self.adaption.get_learning_rate())
            t += 1
        return cluster

    def _get_coordinates_of_index(self, index):
        return self.get_coordinates_of_index(index, self._net_dimensions)

    def get_index_of_coordinates(self, coordinates):
        """
        :param coordinates: float index array
        :return:
        """
        return self._get_index_of_coordinates(
            coordinates,
            self._net_dimensions)

    def _get_index_of_coordinates(self, coordinates, dimensions):
        index = 0
        i = len(dimensions) - 1
        while (i >= 0):
            if (coordinates[i] < 0):
                coordinates[i] = dimensions[i] + coordinates[i]
            index *= dimensions[i]
            index += abs(coordinates[i] % dimensions[i])
            i -= 1
        index = index - 1
        return index

    def get_training(self):
        return self.training

    def get_stimulus_at_row(self, row):
        return self.training[row][:self._data_dimention + 1]

    def count_stimulus(self):
        return len(self.training)

    def get_winner(self, stimulus):
        """
        Get nearest node between weight vector and stimulus
        :param stimulus:
        :return: index node
        """
        minimal = 9999999999999999999
        idx = 0
        var = stimulus[:self._data_dimention + 1]
        euc_obj = EuclideanDistance()
        for i, node_obj in enumerate(self.node):
            result = euc_obj.get_distance(var, node_obj.get_node())
            if result < minimal:
                idx = i
                minimal = result
        return idx

    def get_coordinates_of_index(self, index, dimensions):
        """
        I assume this program only for 2 dimension nodes.
        This method will return coordinate [y x].
        :param index:
        :param dimensions:
        :return:
        """
        len_dimention = len(dimensions)
        if len_dimention > 2:
            raise Exception(
                'This program maximum handle 2 dimension.')
        coordinate = [None] * len_dimention
        for i in range(0, len_dimention):
            coordinate[i] = index % dimensions[i]
            index = index / dimensions[i]
        return coordinate

    def cubehasnext(self):
        """
        Cube counter adalah banyaknya update vector yg telah dilakukan
        contoh vector yg harus di update sebanyak 9 (3^dimensi) jadi cube fungsi
        ini akan mengirimkan False jika counter sudah senilai banyaknya vector
        yg harus di update
        :return:
        """
        if self._cube_counter < math.pow(self._cube_edge_length,
                                         self._net_dimension):
            return True
        else:
            return False

    def cube(self, cubeEdgeLength, offset):
        self._cube_edge_lengths = \
            self._set_array([0] * self._net_dimension, cubeEdgeLength)
        self._cube_edge_length = cubeEdgeLength
        # winner node
        self._cube_offset = offset
        self._cube_counter = 0

    def _cubeNext(self):
        if (self.cubehasnext()):
            # Calculating relative position of node in hypercube
            coordModifier = self.get_coordinates_of_index(
                self._cube_counter, self._cube_edge_lengths)
            # shifting Hypercube, so that it's centered on the stimulus
            coordModifier = self._add_array(coordModifier,
                                             (-1 * self._cube_edge_length) / 2)
            # adding relative Cube coordinates to absolut position of stimulus
            currentCoord = self._add_arrays(coordModifier, self._cube_offset)
            # getting node index in array from absolute coords.
            self._cube_counter += 1
            # print("-> ", currentCoord)
            return self.get_index_of_coordinates(currentCoord)
        else:
            return -1

    def _add_arrays(self, array, adder):
        if len(array) == len(adder):
            for i in range(0, len(array)):
                array[i] = math.floor(array[i] + adder[i])
        return array

    def _add_array(self, array, adder):
        for i in range(0, len(array)):
            array[i] += adder
        return array

    def _set_array(self, array, value):
        for i in range(0, len(array)):
            array[i] = value
        return array

    def show_all_node(self):
        for node_obj in self.node:
            print(node_obj.get_node())
