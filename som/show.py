import numpy as np
import matplotlib.pyplot as plt
from kohonen import KohonenNet


koh_net = KohonenNet(3, 2, [10, 10], 'iris_demo.csv', 200)
res = koh_net.som_impl()
x = []
y = []
x = np.array(x)
y = np.array(y)
N = 50
area = (20 * np.random.rand(N))**2  # 0 to 15 point radii
for i,k in enumerate(res):
    if res[i].get('stimulus')[4] == 'Iris-virginica':
        color = 'r'
    elif res[i].get('stimulus')[4] == 'Iris-setosa':
        color = 'b'
    else:
        color = 'g'
    plt.scatter(res[i].get('cluster')[0], res[i].get('cluster')[1],
                s=area, c=color, alpha=0.5, marker='+')

plt.show()