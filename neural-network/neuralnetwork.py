from abc import ABC, abstractmethod

class NeuralNetwork(ABC):
	
	def __init__(self, value):
		self.value = value
		super().__init__()
	
	@abstractmethod
	def prepare_neuron():
		"""
		Randomly position the grid’s neurons in the data space.
		"""
		pass
	
	@abstractmethod
	def get_closest_neuron():
		"""
		Select one data point, either randomly or systematically 
		cycling through the dataset in order
		"""
		pass
	
	@abstractmethod
	def update_bmu():
		"""
		Find the neuron that is closest to the chosen data point. 
		This neuron is called the Best Matching Unit (BMU).
		"""
		pass
	
	@abstractmethod
	def update_bmu_neighbors():
		"""
		Move the BMU closer to that data point. The distance moved 
		by the BMU is determined by a learning rate, which decreases 
		after each iteration.
		"""
		pass
	
	@abstractmethod
	def update_competitive_params():
		"""
		Update the learning rate and BMU radius, before repeating 
		Steps 1 to 4. Iterate these steps until positions of neurons 
		have been stabilized.
		"""
		pass

# refrerence
# https://algobeans.com/2017/11/02/self-organizing-map/