from som.kohonen import KohonenNet
import unittest


class TestKohonenNet(unittest.TestCase):

    def test_1(self):
        """
        Test get coordinate & test length node after with :
        1. datadimention 3
        2. dimention 2
        3. kohonen net 10 x 10
        :return:
        """
        koh_net = KohonenNet(3, 2, [10, 10], 'iris_demo.csv', 80)
        coord = koh_net._get_coordinates_of_index(10)
        self.assertEqual(coord, [0, 1])
        coord = koh_net._get_coordinates_of_index(49)
        self.assertEqual(coord, [9, 4])
        # 100 kohonen net from 10 x 10
        total_kohonen_net = len(koh_net.node)
        self.assertEqual(100, total_kohonen_net)
        # next test
        train_data = koh_net.get_training()
        dim = 4
        self.assertEqual(
            all([5.1, 3.5, 1.4, 0.2] == train_data[0][:dim]), True)
        self.assertEqual(
            all([4.9, 3.0, 1.4, 0.2] == train_data[1][:dim]), True)
        self.assertEqual(
            all([5.1, 2.5, 3.0, 1.1] == train_data[2][:dim]), True)
        self.assertEqual(
            all([5.7, 2.8, 4.1, 1.3] == train_data[3][:dim]), True)
        self.assertEqual(
            all([6.3, 3.3, 6.0, 2.5] == train_data[4][:dim]), True)
        # next test
        all_true = koh_net.get_stimulus_at_row(2) == [5.1, 2.5, 3.0, 1.1]
        self.assertEqual(all(all_true), True, "Different")
        self.assertEqual(koh_net.count_stimulus(), 5)
        res = koh_net.som_impl()
        print("cluster result:")
        for i,k in enumerate(res):
            print("stimulus         : ", res[i].get('stimulus'))
            print("hasil cluster    : ", res[i].get('cluster'))
            print("hasil SOM        : ", res[i].get('vector'))
            print("==================")

    def test_2(self):
        """
        Test Error dimention must 0 <= x <= 2
        :return:
        """
        with self.assertRaises(Exception):
            KohonenNet(3, 3, [10, 10, 10], 'iris_demo.csv')

    def test_3(self):
        """
        Test 1 dimensi
        :return:
        """
        koh_net = KohonenNet(3, 1, [5], 'iris_demo.csv', 60)
        coord = koh_net._get_coordinates_of_index(10)
        coord = koh_net._get_coordinates_of_index(49)
        # 100 kohonen net from 10 x 10
        total_kohonen_net = len(koh_net.node)
        # next test
        train_data = koh_net.get_training()
        res = koh_net.som_impl()
        print("\n\n\n[TEST 3] cluster result:")
        for i,k in enumerate(res):
            print("stimulus         : ", res[i].get('stimulus'))
            print("hasil cluster    : ", res[i].get('cluster'))
            print("hasil SOM        : ", res[i].get('vector'))
            print("==================")
