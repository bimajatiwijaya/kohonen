from som.euclidean_distance import EuclideanDistance as ED
import unittest


class TestEuclidean(unittest.TestCase):

    def test(self):
        euc_dist = ED()
        dist = euc_dist.get_distance([5.0, 10.0], [10.0, 10.0])
        self.assertEqual(5.0, dist)


if __name__ == '__main__':
    import doctest
    doctest._test()