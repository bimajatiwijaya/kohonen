from som.node import KohonenNode
import unittest


class TestKohonen(unittest.TestCase):

    def setUp(self):
        self.node = [1.0, 1.2, 4.5]
        self.khn_node = KohonenNode(self.node)

    def test_initial(self):
        test_get = self.khn_node.get_node()
        for i in range(0, 3):
            self.assertEqual(test_get[i], self.node[i])

    def test_update_node(self):
        """
        all adaption class attribute using default value
        :return:
        """
        stimulus = [2, 10, 9]
        self.khn_node.update_weight1(stimulus)
        """
        expected_1 = 1.0 + 0.3 * 5 * (2 - 1.0)
        expected_2 = 1.2 + 0.3 * 5 * (10 - 1.2)
        expected_1 = 4.5 + 0.3 * 5 * (9 - 4.5)
        """
        expected_weight = [2.5, 14.4, 11.25]
        for i in range(0, 3):
            self.assertEqual(expected_weight[i], self.khn_node.node[i])


if __name__ == '__main__':
    import doctest
    doctest._test()